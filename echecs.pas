
{
  Programme de Luc Rousseau
  https://github.com/Loutcho/turbopascal
  Version retouch�e pour l'unit� ptcGraph
}

program Echecs;

uses
{$IFDEF unix}
  cThreads,
{$ENDIF}
  ptcCrt, ptcGraph, Constantes, Dessin;

type
  Grille = array[0..7, 0..7] of byte;

  (*
  ���������������������������������
  �   �   �   �   �   �   �   �   �
  ����+���+���+���+���+���+���+���+
  �abg�e.p�s�l�couleur� pi�ce     �
  *)
  
var
  g: grille;
  x, y, xs, ys: shortint;
  quitter, fini, pselect: boolean;

procedure ModeGraphique;
var
  gd, gm: smallint;
begin
  gd := VGA;
  gm := VGAHI;
  WindowTitle := 'Echecs ptcGraph';
  InitGraph(gd, gm, '');
end;

function CouleurCase(x, y: shortint): byte;
begin
  case (x + y) mod 2 of
    0: CouleurCase := Noir;
    1: CouleurCase := Blanc;
  end;
end;

function XX(x: shortint): integer;
begin
  xx := GaucheEchiquier + LargeurCase * x;
end;

function YY(y: shortint): integer;
begin
  yy := HauteurCase * (8 - y) + HautEchiquier;
end;

procedure Place(x, y, c: shortint);
var
  cc: byte;
  x1, x2, y1, y2, x0, y0: integer;
begin
  case CouleurCase(x, y) of
    Noir:  cc := DarkGray;
    Blanc: cc := LightGray;
  end;
  SetFillStyle(SolidFill, cc);
  x1 := xx(x);
  y1 := yy(y);
  x2 := xx(x + 1);
  y2 := yy(y + 1);
  x0 := (x1 + x2) div 2;
  y0 := (y1 + y2) div 2;
  Bar(x1, y1, x2 - 1, y2 + 1);
  case c and Couleur of
    Blanc: cc := Yellow;
    Noir:  cc := Brown;
  end;
  SetColor(cc);
  SetFillStyle(SolidFill, cc);
  DessinePiece(c and Piece, x0, y0);
end;

procedure Curseur(x, y, c: shortint);
var
  x1, x2, y1, y2: integer;
  cc: byte;
const
  e = 1;
  f = 3;
begin
  x1 := xx(x);
  y1 := yy(y);
  x2 := xx(x + 1) - 1;
  y2 := yy(y + 1) + 1;
  case c and Couleur of
    Rien: if CouleurCase(x, y) = Blanc then cc := LightGray else cc := DarkGray;
    Blanc: cc := White;
    Noir: cc := Black;
  end;
  SetFillStyle(SolidFill, cc);
  if c and 32 = 0 then
  begin
    Bar(x1 + e, y1 - e, x2 - e, y1 - f);
    Bar(x1 + e, y2 + e, x2 - e, y2 + f);
    Bar(x1 + e, y1 - e, x1 + f, y2 + e);
    Bar(x2 - e, y1 - e, x2 - f, y2 + e);
  end else
  begin
    Bar(x1 + e, y1 - e, x2 - e, y1 - f - 1);
    Bar(x1 + e, y2 + e, x2 - e, y2 + f + 1);
    Bar(x1 + e, y1 - e, x1 + f + 1, y2 + e);
    Bar(x2 - e, y1 - e, x2 - f - 1, y2 + e);
  end;
end;

procedure TraceGrille;
var
  i, j: shortint;
begin
  SetColor(White);
  //Rectangle(0, 0, 639, 479);
  SetTextJustify(CenterText, CenterText);
  SetTextStyle(TriplexFont, VertDir, 9);
  OutTextXY(558, 220, 'ECHECS');
  SetTextStyle(DefaultFont, VertDir, 1);
  OutTextXY(630, 220, 'Programmation par Luc ROUSSEAU (27 ao�t 1996)');
  SetTextStyle(GothicFont, HorizDir, 4);
  for i := 0 to 7 do
    for j := 0 to 7 do
      Place(i, j, g[i, j]);
  SetColor(White);
  SetTextStyle(TriplexFont, HorizDir, 4);
  for i := 0 to 7 do
  begin
    MoveTo(LargeurCase div 2, yy(i) - HauteurCase div 2);
    OutText(chr(i + ord('1')));
  end;
  for i := 0 to 7 do
  begin
    MoveTo(xx(i) + LargeurCase div 2, 8 * HauteurCase + HautEchiquier + HauteurCase div 2);
    OutText(chr(i + ord('A')));
  end;
end;

procedure InitGrille(var g: grille);
var
  x, y: shortint;
begin
  for x := 0 to 7 do
  begin
    for y := 2 to 5 do
      g[x, y] := Rien;
    g[x, 1] := Pion + Blanc;
    g[x, 6] := Pion + Noir;
  end;
  g[0, 0] := Tour + Blanc;
  g[1, 0] := Cavalier + Blanc;
  g[2, 0] := Fou + Blanc;
  g[3, 0] := Dame + Blanc;
  g[4, 0] := Roi + Blanc;
  g[5, 0] := Fou + Blanc;
  g[6, 0] := Cavalier + Blanc;
  g[7, 0] := Tour + Blanc;
  g[0, 7] := Tour + Noir;
  g[1, 7] := Cavalier + Noir;
  g[2, 7] := Fou + Noir;
  g[3, 7] := Dame + Noir;
  g[4, 7] := Roi + Noir;
  g[5, 7] := Fou + Noir;
  g[6, 7] := Cavalier + Noir;
  g[7, 7] := Tour + Noir;
end;

procedure Info;
begin
end;

function Ennemi(x: shortint): shortint;
begin
  case x of
    Noir: ennemi := Blanc;
    Blanc: ennemi := Noir;
  else
    ennemi := Rien;
  end;
end;

procedure Effectue(x1, y1, x2, y2: shortint; special: char);
var
  y: shortint;
begin
  g[x2, y2] := g[x1, y1] xor Select;
  g[x1, y1] := Rien;
  case special of
    'c', 'C': g[x2, y2] := (g[x2, y2] and $F8) or Cavalier;
    'f', 'F': g[x2, y2] := (g[x2, y2] and $F8) or Fou;
    't', 'T': g[x2, y2] := (g[x2, y2] and $F8) or Tour;
    'd', 'D': g[x2, y2] := (g[x2, y2] and $F8) or Dame;
    'e':
      begin
        if (y2 - y1) = 2 then
          y := y1 + 1
        else
          y := y1 - 1;
        g[x2, y] := EnPassant;
      end;
    'E':
      begin
        g[x2, y1] := Rien;
        Place(x2, y1, g[x2, y1]);
      end;
  end;
  Place(x2, y2, g[x2, y2]);
  Place(x1, y1, g[x1, y1]);
end;

procedure EffectueVirtuel(x1, y1, x2, y2: shortint; special: char; var g: grille);
var
  y: shortint;
begin
  g[x2, y2] := g[x1, y1];
  g[x1, y1] := Rien;
  case special of
    'c', 'C': g[x2, y2] := (g[x2, y2] and $F8) or Cavalier;
    'f', 'F': g[x2, y2] := (g[x2, y2] and $F8) or Fou;
    't', 'T': g[x2, y2] := (g[x2, y2] and $F8) or Tour;
    'd', 'D': g[x2, y2] := (g[x2, y2] and $F8) or Dame;
    'e':
      begin
        y := (y1 + y2) div 2;
        g[x2, y] := EnPassant;
      end;
    'E':
      begin
        g[x2, y1] := Rien;
      end;
  end;
end;

function PionPossible(x1, y1, x2, y2, trait: shortint; var special: char): boolean;
var
  iy: shortint;
  f: boolean;
begin
  f := false;
  if g[x1, y1] and Couleur = Blanc then
    iy := 1
  else
    iy := -1;
  if (g[x2, y2] and Couleur = ennemi(trait))
    or (g[x2, y2] and EnPassant <> 0) then
  begin
    f := (abs(x2 - x1) = 1) and (y2 - y1 = iy);
    if g[x2, y2] and EnPassant <> 0 then
      special := 'E';
  end else
  begin
    f := (x2 - x1) = 0;
    if f then
      case iy * (y2 - y1) of
        1:
          f := true;
        2:
          begin
            f := (g[x1, y1 + iy] = Rien) and (((y1 = 1) and (trait = Blanc)) or ((y1 = 6) and (trait = Noir)));
            if f then
              special := 'e';
          end;
      else
        f := false;
      end;
  end;
  if f then
    if (((trait = Blanc) and (y2 = 7)) or ((trait = Noir) and (y2 = 0))) then
      repeat
        special := readkey;
      until upcase(special) in ['C', 'F', 'T', 'D'];
  PionPossible := f;
end;

function CavalierPossible(x1, y1, x2, y2: shortint): boolean;
var
  f: boolean;
  dx, dy: shortint;
begin
  dx := abs(x2 - x1);
  dy := abs(y2 - y1);
  f := ((dx = 1) and (dy = 2)) or ((dx = 2) and (dy = 1));
  CavalierPossible := f;
end;

function FouPossible(x1, y1, x2, y2: shortint): boolean;
var
  f, ff: boolean;
  dx, dy: shortint;
  x, y: shortint;
begin
  dx := x2 - x1;
  dy := y2 - y1;
  f := abs(dx) = abs(dy);
  if f then
  begin
    if dx > 0 then
      dx := 1
    else
      dx := -1;
    if dy > 0 then
      dy := 1
    else
      dy := -1;
    x := x1;
    y := y1;
    repeat x := x + dx;
      y := y + dy;
      f := g[x, y] and Piece = Rien;
      ff := (x = x2) and (y = y2);
    until (not f) or ff;
    if ff then
      f := true;
  end;
  FouPossible := f;
end;

function TourPossible(x1, y1, x2, y2: shortint): boolean;
var
  f, ff: boolean;
  dx, dy: shortint;
  x, y: shortint;
begin
  dx := x2 - x1;
  dy := y2 - y1;
  f := ((dx = 0) and (dy <> 0)) or ((dx <> 0) and (dy = 0));
  if f then
  begin
    if dx > 0 then
      dx := 1
    else if dx < 0 then
      dx := -1
    else
      dx := 0;
    if dy > 0 then
      dy := 1
    else if dy < 0 then
      dy := -1
    else
      dy := 0;
    x := x1;
    y := y1;
    repeat x := x + dx;
      y := y + dy;
      f := g[x, y] and Piece = Rien;
      ff := (x = x2) and (y = y2);
    until (not f) or ff;
    if ff then
      f := true;
  end;
  TourPossible := f;
end;

function DamePossible(x1, y1, x2, y2: shortint): boolean;
var
  f, ff: boolean;
  dx, dy: shortint;
  x, y: shortint;
begin
  dx := x2 - x1;
  dy := y2 - y1;
  f := ((dx = 0) and (dy <> 0)) or ((dx <> 0) and (dy = 0))
    or (abs(dx) = abs(dy));
  if f then
  begin
    if dx > 0 then
      dx := 1
    else if dx < 0 then
      dx := -1
    else
      dx := 0;
    if dy > 0 then
      dy := 1
    else if dy < 0 then
      dy := -1
    else
      dy := 0;
    x := x1;
    y := y1;
    repeat x := x + dx;
      y := y + dy;
      f := g[x, y] and Piece = Rien;
      ff := (x = x2) and (y = y2);
    until (not f) or ff;
    if ff then
      f := true;
  end;
  DamePossible := f;
end;

function RoiPossible(x1, y1, x2, y2, trait: shortint; var special: char): boolean;
var
  dx, dy: shortint;
  f: boolean;
begin
  dx := abs(x2 - x1);
  dy := abs(y2 - y1);
  if (dx <= 1) and (dy <= 1) then
    f := true
  else if (dy <> 0) or (dx <> 2) then
    f := false
  else (* Il s'agit du roque *)
  begin
    f := true {pour l'instant}
  end;
  RoiPossible := f;
end;

function CoupPossible(x1, y1, x2, y2, trait: shortint;
  var special: char; g: grille): boolean;
var
  f: boolean;
const
  EchecSurFuturePosition = false;
begin
  f := false;
  if g[x2, y2] and Couleur = trait then
    f := false
  else
  begin
    case g[x1, y1] and Piece of
      Pion: f := PionPossible(x1, y1, x2, y2, trait, special);
      Cavalier: f := CavalierPossible(x1, y1, x2, y2);
      Fou: f := FouPossible(x1, y1, x2, y2);
      Tour: f := TourPossible(x1, y1, x2, y2);
      Dame: f := DamePossible(x1, y1, x2, y2);
      Roi: f := RoiPossible(x1, y1, x2, y2, trait, special);
    end;
    if f then
    begin
      EffectueVirtuel(x1, y1, x2, y2, special, g)
     {f := not EchecSurFuturePosition;}
    end;
  end;
  CoupPossible := f;
end;

procedure RetireEP(trait: shortint);
var
  x, y: shortint;
begin
  if trait = Blanc then
    y := 2
  else
    y := 5;
  for x := 0 to 7 do
    g[x, y] := g[x, y] and ($FF xor EnPassant);
end;

procedure GC(var quitter: boolean; var g: grille; var x, y, trait: shortint; var fini, pselect: boolean; var xs, ys: shortint);
var
  t, special: char;
begin
  t := readkey;
  if t <> #0 then
  begin
    t := upcase(t);
    case t of
      #27:
        quitter := true;
      #13:
        if not pselect then (* pas de pi�ce en main *)
        begin
          if (g[x, y] <> Rien)
            and (g[x, y] and Couleur = trait) then (* s�lection pi�ce amie *)
          begin
            pselect := true;
            xs := x;
            ys := y;
            g[x, y] := g[x, y] xor Select;
            Curseur(x, y, g[x, y]);
          end;
        end
        else (* d�j� une pi�ce en main *)
        begin
          if (x = xs) and (y = ys) then (* d�s�lection pi�ce *)
          begin
            g[x, y] := g[x, y] xor Select;
            Place(x, y, g[x, y]);
            Curseur(x, y, g[x, y]);
            pselect := false;
          end
          else (* �tude du coup *)
          begin
            special := ' ';
            if CoupPossible(xs, ys, x, y, trait, special, g) then
            begin
              Effectue(xs, ys, x, y, special);
              pselect := false;
              trait := ennemi(trait);
              RetireEP(trait);
              Curseur(x, y, trait);
            end;
          end;
        end;
    end; (* Case *)
  end (* if *)
  else
  begin
    t := readkey;
    case t of
      #75:
        begin
          Curseur(x, y, Rien);
          if x > 0 then
            x := x - 1
          else
            x := 7;
          Curseur(x, y, trait);
        end; (* #75 *)
      #77:
        begin
          Curseur(x, y, Rien);
          if x < 7 then
            x := x + 1
          else
            x := 0;
          Curseur(x, y, trait);
        end; (* #77 *)
      #72:
        begin
          Curseur(x, y, Rien);
          if y < 7 then
            y := y + 1
          else
            y := 0;
          Curseur(x, y, trait);
        end; (* #72 *)
      #80:
        begin
          Curseur(x, y, Rien);
          if y > 0 then
            y := y - 1
          else
            y := 7;
          Curseur(x, y, trait);
        end; (* #80 *)
    end; (* Case *)
  end; (* Else *)
end;

var
  trait: shortint;
  
begin
  InitGrille(g);
  x := 4;
  y := 1;
  trait := Blanc;
  quitter := false;
  fini := false;
  pselect := false;
  ModeGraphique;
  SetRGBPalette(EGADarkGray, 40, 34, 34);
  SetRGBPalette(EGALightGray, 44, 40, 40);
  SetRGBPalette(EGABrown, 25, 20, 15);
  SetRGBPalette(EGAYellow, 63, 63, 40);
  TraceGrille;
  Curseur(x, y, trait);
  Info;
  while not Quitter and not Fini do
  begin
    GC(quitter, g, x, y, trait, fini, pselect, xs, ys);
  end;
  if Fini then
    ReadKey;
  RestoreCRTMode;
  CloseGraph;
end.
