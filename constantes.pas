
unit Constantes;

interface

const
  Piece = 7;
  Rien = 0;
  Pion = 1;
  Cavalier = 2;
  Fou = 3;
  Tour = 4;
  Dame = 5;
  Roi = 6;
  
  Couleur = 24;
  Blanc = 8;
  Noir = 16;
  Select = 32;
  EnPassant = 64;
  
  LargeurCase = 54;
  HauteurCase = 54;
  Bord = 2;
  LargeurLettre = 44;
  GaucheEchiquier = LargeurLettre + Bord;
  HautEchiquier = Bord;

implementation

end.
