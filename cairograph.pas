
unit CairoGraph;

interface

uses
  Cairo;
  
const
  SolidLn = 0;
  DashedLn = 1;
  ThickWidth = 0;

procedure Arc(x, y: double; start, stop, radius: word);
procedure Circle(x, y: double; radius: word);
procedure Line(x1, y1, x2, y2: double);
procedure Rectangle(x1, y1, x2, y2: double);
procedure SetLineStyle(linestyle, pattern, thickness: word);

var 
  context: pcairo_t;
  
implementation

uses
  Math;
  
procedure Arc(x, y: double; start, stop, radius: word);
begin
  cairo_arc(context, x, y, radius, DegToRad(start), DegToRad(stop)); 
  cairo_stroke(context);
end;

procedure Circle(x, y: double; radius: word);
begin
  cairo_arc(context, x, y, radius, 0, 2 * PI); 
  cairo_stroke(context);
end;

procedure Line(x1, y1, x2, y2: double);
begin
  cairo_move_to(context, x1, y1);
  cairo_line_to(context, x2, y2);
  cairo_stroke(context);
end;

procedure Rectangle(x1, y1, x2, y2: double);
begin
  cairo_rectangle(context, x1, y1, x2 - x1, y2 - y1);
  cairo_stroke(context);
end;

procedure SetLineStyle(linestyle, pattern, thickness: word);
const
  dashed: array [0..1] of double = (2, 6); 
begin
  case linestyle of
    SolidLn: cairo_set_dash(context, nil, 0, 0);
    DashedLn: cairo_set_dash(context, dashed, 2, 0);
  end;
end;

end.

