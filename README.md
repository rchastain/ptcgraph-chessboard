# ptcGraph Chessboard

Chess pieces created for Turbo Pascal Graph unit by Luc Rousseau: https://github.com/Loutcho/turbopascal

![Screenshot](screenshot.png)
