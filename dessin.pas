
unit Dessin;

interface

procedure DessinePiece(piece: byte; x, y: {$IFDEF CAIROGRAPH}double{$ELSE}integer{$ENDIF});

implementation

uses
{$IFDEF CAIROGRAPH}
  Cairo, CairoGraph,
{$ELSE}
  ptcGraph,
{$ENDIF}
  Constantes;

{$IFDEF CAIROGRAPH}
const
  SURFACE_WIDTH = 4 * LargeurCase;
  SURFACE_HEIGHT = 4 * HauteurCase;
  NOM_PIECE: array[Rien..Roi] of string = ('Rien', 'Pion', 'Cavalier', 'Fou', 'Tour', 'Dame', 'Roi');
var
  surface: pcairo_surface_t;
{$ENDIF}

procedure DessinePiece(piece: byte; x, y: {$IFDEF CAIROGRAPH}double{$ELSE}integer{$ENDIF});
begin
{$IFDEF CAIROGRAPH}
  surface := cairo_image_surface_create(CAIRO_FORMAT_ARGB32, SURFACE_WIDTH, SURFACE_HEIGHT);
  context := cairo_create(surface);
  cairo_set_line_cap(context, CAIRO_LINE_CAP_ROUND);
  cairo_set_line_join(context, CAIRO_LINE_JOIN_ROUND);
  cairo_translate(context, SURFACE_WIDTH div 2, SURFACE_HEIGHT div 2);
  cairo_scale(context, SURFACE_WIDTH div LargeurCase, SURFACE_HEIGHT div HauteurCase);
  cairo_set_line_width(context, 2.2);
  cairo_set_source_rgba(context, 0, 0, 0, 1); 
{$ENDIF}
  SetLineStyle(SolidLn, 0, ThickWidth);
  case piece of
    Pion:
      begin
        Circle(x, y - 13, 5);
        Line(x - 10, y - 4, x + 10, y - 4);
        Rectangle(x - 6, y, x + 6, y + 12);
        Line(x - 10, y + 16, x + 10, y + 16);
        Line(x - 10, y + 20, x + 10, y + 20);
      end;
    Cavalier:
      begin
        Arc(x - 7, y - 6, 90, 270, 6);
        Line(x - 7, y - 12, x, y - 12);
        Line(x - 7, y, x, y);
        Arc(x, y, 270, 90, 12);
        Line(x, y, x - 6, y + 12);
        Line(x - 6, y + 12, x, y + 12);
        Line(x - 10, y + 16, x + 10, y + 16);
        Line(x - 10, y + 20, x + 10, y + 20);
        Line(x + 3, y - 12, x + 6, y - 15);
        Line(x + 7, y - 10, x + 10, y - 14);
        Line(x + 10, y - 14, x + {10}11, y - 6);
      end;
    Fou:
      begin
        Circle(x, y - 15, 2);
        Arc(x + {6}5, y, 110, 240, 13);
        Arc(x - 5, y, 300, 70, 13);
        cairo_set_line_cap(context, CAIRO_LINE_CAP_SQUARE);
        Line(x - 4{$IFDEF CAIROGRAPH}{ + 0.18}{$ENDIF}, y, x + 4{$IFDEF CAIROGRAPH}{ + 0.18}{$ENDIF}, y);
        Line(x{$IFDEF CAIROGRAPH}{ + 0.18}{$ENDIF}, y - 4, x{$IFDEF CAIROGRAPH}{ + 0.18}{$ENDIF}, y + 4);
        cairo_set_line_cap(context, CAIRO_LINE_CAP_ROUND);
        Line(x - 6, y + 14, x + 6, y + 14);
        Line(x - 6, y + 20, x - 2, y + 16);
        Line(x + 6, y + 20, x + 2, y + 16);
        Line(x - 6, y + 20, x - 16, y + 20);
        Line(x + 6, y + 20, x + 16, y + 20);
      end;
    Tour:
      begin
        Rectangle(x - 8, y - 8, x + 8, y + 12);
        Line(x - 12, y + 16, x + 12, y + 16);
        Line(x - 12, y + 20, x + 12, y + 20);
        Line(x - 12, y - 12, x + 12, y - 12);
        SetLineStyle(DashedLn, 0, ThickWidth);
        Line(x - 13, y - 16, x + 13, y - 16);
      end;
    Dame:
      begin
        Arc(x - 9, y + 3, {30}33, {330}327, 11);
        Arc(x + 9, y + 3, {210}213, {150}147, 11);
        Line(x - 12, y + 16, x + 12, y + 16);
        Line(x - 12, y + 20, x + 12, y + 20);
        Circle(x, y - 12, 6);
      end;
    Roi:
      begin
        Arc(x - 9, y + 3, {30}33, {330}327, 11);
        Arc(x + 9, y + 3, {210}213, {150}147, 11);
        Line(x - 12, y + 16, x + 12, y + 16);
        Line(x - 12, y + 20, x + 12, y + 20);
        Line(x, {y}y - 5, x, y - 20);
        Line(x - 5, y - 15, x + 5, y - 15);
      end;
  end;
{$IFDEF CAIROGRAPH}
  cairo_surface_write_to_png(surface, pchar(NOM_PIECE[piece] + '.png'));
  cairo_destroy(context);
  cairo_surface_destroy(surface);
{$ENDIF}
end;

end.
